Vioside CallOut
===============

Introduction
------------
This application incorporates the VioSkeleton application together with the CallOut application for sending out push notifications using the Apple Push Notification and Google Play GCM services by Zend Framework 2 Services. It also incorporates a UI to allow users to login and send a broadcast notification.

Otherwise the CallOut module can be copied on it's own but these steps still need to be followed for installation.


Installation
------------

Using Composer (recommended)
----------------------------
The recommended way to get a working copy of this project is to clone the repository
and use `composer` to install dependencies using the `create-project` command:


    cd my/project/dir
    git clone git://github.com/vioside/CallOut.git
    cd VioSkeleton
    sudo php composer.phar self-update
    sudo php composer.phar install


Virtual Host
------------
Afterwards, set up a virtual host to point to the root directory of the
project. An .htaccess file is required in the root directory to revert to the public dir.
This file is included in the project


Database
--------
A database is required to setup a table for the users and tokens to be stored on the server.
Setup the local.php file /config/autoload with the following values:

```php
    'db' => array(
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=<dbname>;host=localhost',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
        'username' => '<username>',
        'password' => '<password>',
    ),
```

And use the following to create the token and user tables

```mysql
CREATE TABLE `token` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `username` varchar(30) default NULL,
  `token` varchar(300) default NULL,
  `ios` tinyint(1) default NULL,
  `lastchanged` int(20) default NULL,
  `tags` varchar(40) default NULL,
  `active` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `display_name` varchar(50) default NULL,
  `password` varchar(128) NOT NULL,
  `state` smallint(6) default NULL,
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `calloutreport` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `date` date default NULL,
  `noSent` int(11) default NULL,
  `ios` int(11) default NULL,
  `token` varchar(200) default NULL,
  `invalid` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
```

Use an md5 password generator to create the user


Styling
-------
Copy the callout-assets directory from the public directory of the module to the root public directory.
Copy the layout file from the callout-assets to the application layout directory.

Create an icon and upload it to /callout-assets/icon.png in the root public directory


Certificates and API Keys
-------------------------
Upload the APNS Certificates to the CallOut module directory at 'Certificates' as:
- DevCertificate.pem
- ProCertificate.pem

Switch between production to certificate by commenting the appropriates lines in the AppleTokenTable.php file

Change the Google API Key from the GoogleTokenTable.php file in the CallOut module directory
