<?php
namespace Requests;

use Requests\Model\Requests;
use Requests\Model\RequestsTable;
use Requests\Model\Quotes;
use Requests\Model\QuotesTable;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Requests\Model\RequestsTable' =>  function($sm) {
                    $tableGateway = $sm->get('RequestsTableGateway');
                    $table = new RequestsTable($tableGateway);
                    return $table;
                },
                'RequestsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Requests());
                    return new TableGateway('requests', $dbAdapter, null, $resultSetPrototype);
                },
                'Requests\Model\QuotesTable' =>  function($sm) {
                    $tableGateway = $sm->get('QuotesTableGateway');
                    $table = new QuotesTable($tableGateway);
                    return $table;
                },
                'QuotesTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Quotes());
                    return new TableGateway('quote', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}