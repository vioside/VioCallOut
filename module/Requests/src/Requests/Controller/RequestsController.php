<?php
namespace Requests\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class RequestsController extends AbstractActionController
{
/*
	protected $requestsTable;
	
    public function getRequestsTable()
    {
        if (!$this->requestsTable) {
            $sm = $this->getServiceLocator();
            $this->requestsTable = $sm->get('Requests\Model\RequestsTable');
        }
        return $this->requestsTable;
    }
    */
    public function indexAction()
    {
    	return new ViewModel();
    }
}