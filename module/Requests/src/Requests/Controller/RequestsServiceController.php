<?php
namespace Requests\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class RequestsServiceController extends AbstractRestfulController
{
	protected $requestsTable;

	//link to Requests Table found in Model
	public function getRequestsTable()
    {
        if (!$this->requestsTable) {
            $sm = $this->getServiceLocator();
            $this->requestsTable = $sm->get('Requests\Model\RequestsTable');
        }
        return $this->requestsTable;
    }
    
    
    //calling fetchAll method founf in RequestsTable.php 
	public function getList(){
		$requests = $this->getRequestsTable()->fetchAll();
		$data = array();
		$i = 0;
		foreach ($requests as $request) {
			$data[$i] = $request;
			$i = $i + 1;
		}

		return new JsonModel( $data );
	}

	public function get($id) {
		$request = $this->getRequestsTable()->getRequests($id);
		$data = array('request'=>$request);
		return new JsonModel($data);
	}

	public function create($data) { 
/* 		return new JsonModel($data); */
	}

	public function update($id, $data) {
		$request = $this->getRequestsTable()->getRequests($id);
		if(array_key_exists("vehicleType", $data)) $request->vehicleType = $data['vehicleType'];
		$this->getRequestsTable()->saveRequests($request);
		
		$d = array('request' => $request);
		return new JsonModel($d);
	}

	public function delete($id) { }
}