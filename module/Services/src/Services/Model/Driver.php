<?php
namespace Services\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use \DateTime;

class Driver implements InputFilterAwareInterface
{
	public $id;
	public $username;
	public $email;
	public $display_name;
	public $password;
	public $state;
	public $surname;
	public $name;
	public $contactno;
	public $vehicleno;
	public $photo;
	public $paid;
	public $country;
	public $vehicleType;
	public $noOfTokens;
	public $lastLatitude;
	public $lastLongitude;
	public $secretCode;
	public $secretCodeEnable;
	public $devicetoken;
	public $android;
	public $lastChangedName;
	public $lastChangedDate;
	public $addedBy;
	public $current;
	public $firstAddedDate;
	public $emailBackup;
	
	protected $inputFilter;


	public function exchangeArray($data)
    {
        $this->id = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->username = (!empty($data['username'])) ? $data['username'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->display_name = (!empty($data['display_name'])) ? $data['display_name'] : null;
        $this->password = (!empty($data['password'])) ? $data['password'] : null;
        $this->state = (!empty($data['state'])) ? $data['state'] : null;
        $this->surname = (!empty($data['surname'])) ? $data['surname'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->contactno = (!empty($data['contactno'])) ? $data['contactno'] : null;
        $this->vehicleno = (!empty($data['vehicleno'])) ? $data['vehicleno'] : null;
        $this->photo = (!empty($data['photo'])) ? $data['photo'] : null;
        $this->paid = (!empty($data['paid'])) ? $data['paid'] : null;
        $this->country = (!empty($data['country'])) ? $data['country'] : null;
        $this->vehicleType = (!empty($data['vehicletype'])) ? $data['vehicletype'] : null;
        $this->noOfTokens = (!empty($data['nooftokens'])) ? $data['nooftokens'] : null;
        $this->lastLatitude = (!empty($data['lastlatitude'])) ? $data['lastlatitude'] : null;
        $this->lastLongitude = (!empty($data['lastlongitude'])) ? $data['lastlongitude'] : null;
        $this->secretCode = (!empty($data['secretcode'])) ? $data['secretcode'] : null;
        $this->secretCodeEnable = (!empty($data['secretcodeenable'])) ? $data['secretcodeenable'] : null;
        $this->devicetoken = (!empty($data['devicetoken'])) ? $data['devicetoken'] : null;
        $this->android = (!empty($data['android'])) ? $data['android'] : null;
        $this->lastChangedName = (!empty($data['lastChangedName'])) ? $data['lastChangedName'] : null;
        $this->lastChangedDate = (!empty($data['lastChangedDate'])) ? $data['lastChangedDate'] : null;
        $this->addedBy = (!empty($data['addedBy'])) ? $data['addedBy'] : null;
        $this->current = (!empty($data['current'])) ? $data['current'] : null;
        $this->firstAddedDate = (!empty($data['firstAddedDate'])) ? $data['firstAddedDate'] : null;
        $this->emailBackup = (!empty($data['emailBackup'])) ? $data['emailBackup'] : null;

    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
    	if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //fields

             $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}