<?php
namespace Services\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Services\Model\Driver;

class DriversController extends AbstractRestfulController
{
	protected $driverTable;

	public function getDriverTable()
    {
        if (!$this->driverTable) {
            $sm = $this->getServiceLocator();
            $this->driverTable = $sm->get('Services\Model\DriverTable');
        }
        return $this->driverTable;
    }
    
    public function getList(){
		$drivers = $this->getDriverTable()->fetchAll();
		$data = array();
		$i = 0;
		foreach ($drivers as $driver) {
			$data[$i] = $driver;
			$i = $i + 1;
		}

		return new JsonModel( $data );
	}
	
	public function get($id) {
		$drivers = $this->getDriverTable()->getDrivers($id);
		$data = array('driver'=>$drivers);
		return new JsonModel($data);
	}
	
	public function create($data) { 
		$driver = new Driver();
		
		if(array_key_exists("username", $data)) $driver->username = $data["username"];
		if(array_key_exists("email", $data)) $driver->email = $data["email"];
		if(array_key_exists("display_name", $data)) $driver->display_name = $data["display_name"];
		if(array_key_exists("password", $data)) $driver->password = $data["password"];
		if(array_key_exists("state", $data)) $driver->state = $data["state"];
		if(array_key_exists("surname", $data)) $driver->surname = $data["surname"];
		if(array_key_exists("name", $data)) $driver->name = $data["name"];
		if(array_key_exists("contactno", $data)) $driver->contactno = $data["contactno"];
		if(array_key_exists("vehicleno", $data)) $driver->vehicleno = $data["vehicleno"];
		if(array_key_exists("photo", $data)) $driver->photo = $data["photo"];
		if(array_key_exists("paid", $data)) $driver->paid = $data["paid"];
		if(array_key_exists("country", $data)) $driver->country = $data["country"];
		if(array_key_exists("vehicletype", $data)) $driver->vehicleType = $data["vehicletype"];
		if(array_key_exists("nooftokens", $data)) $driver->noOfTokens = $data["nooftokens"];
		if(array_key_exists("lastLatitude", $data)) $driver->lastLatitude = $data["lastLatitude"];
		if(array_key_exists("lastLongitude", $data)) $driver->lastLongitude = $data["lastLongitude"];
		if(array_key_exists("secretcode", $data)) $driver->secretCode = $data["secretcode"];
		if(array_key_exists("secretcodeenable", $data)) $driver->secretCodeEnable = $data["secretcodeenable"];
		if(array_key_exists("devicetoken", $data)) $driver->devicetoken = $data["devicetoken"];
		if(array_key_exists("android", $data)) $driver->android = $data["android"];
		$driver->lastChangedDate = date("Y-m-d H:i:s");
		$driver->current = 1;
		$driver->firstAddedDate = date("Y-m-d H:i:s");
		
		$this->getDriverTable()->saveDrivers($driver);
				
		return new JsonModel(array('driver'=>$driver));
	}
	
	public function update($id, $data) {
		
		$driver = $this->getDriverTable()->getDrivers($id);
		
		if(array_key_exists("username", $data)) $driver->username = $data["username"];
		if(array_key_exists("email", $data)) $driver->email = $data["email"];
		if(array_key_exists("display_name", $data)) $driver->display_name = $data["display_name"];
		if(array_key_exists("password", $data)) $driver->password = $data["password"];
		if(array_key_exists("state", $data)) $driver->state = $data["state"];
		if(array_key_exists("surname", $data)) $driver->surname = $data["surname"];
		if(array_key_exists("name", $data)) $driver->name = $data["name"];
		if(array_key_exists("contactno", $data)) $driver->contactno = $data["contactno"];
		if(array_key_exists("vehicleno", $data)) $driver->vehicleno = $data["vehicleno"];
		if(array_key_exists("photo", $data)) $driver->photo = $data["photo"];
		if(array_key_exists("paid", $data)) $driver->paid = $data["paid"];
		if(array_key_exists("country", $data)) $driver->country = $data["country"];
/* 		if(array_key_exists("vehicletype", $data)) $driver->vehicleType = $data["vehicletype"]; */
		if(array_key_exists("nooftokens", $data)) $driver->noOfTokens = $data["nooftokens"];
		if(array_key_exists("lastLatitude", $data)) $driver->lastLatitude = $data["lastLatitude"];
		if(array_key_exists("lastLongitude", $data)) $driver->lastLongitude = $data["lastLongitude"];
		if(array_key_exists("secretcode", $data)) $driver->secretCode = $data["secretcode"];
		if(array_key_exists("secretcodeenable", $data)) $driver->secretCodeEnable = $data["secretcodeenable"];
		if(array_key_exists("devicetoken", $data)) $driver->devicetoken = $data["devicetoken"];
		if(array_key_exists("android", $data)) $driver->android = $data["android"];
		if(array_key_exists("lastChangedDate", $data)) $driver->lastChangedDate = $data["lastChangedDate"];
		if(array_key_exists("current", $data)) $driver->current = $data["current"];   
		if(array_key_exists("firstAddedDate", $data)) $driver->firstAddedDate = $data["firstAddedDate"];   

		$this->getDriverTable()->saveDrivers($driver);
		
		$d = array('driver' => $driver);
		return new JsonModel($d);

	}

	public function delete($id) { }
}