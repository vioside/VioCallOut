<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Services\Controller\Calls' => 'Services\Controller\CallsController',
            'Services\Controller\SendCall' => 'Services\Controller\SendCallController',
             'Services\Controller\Ratings' => 'Services\Controller\RatingsController',
             'Services\Controller\RoadClients' => 'Services\Controller\RoadClientsController',
             'Services\Controller\Drivers' => 'Services\Controller\DriversController',
             'Services\Controller\DriversSC' => 'Services\Controller\DriversSCController',
             'Services\Controller\DriversSCExists' => 'Services\Controller\DriversSCExistsController',
             'Services\Controller\DriversClosest' => 'Services\Controller\DriversClosestController',
             'Services\Controller\CallsDrivers' => 'Services\Controller\CallsDriversController',
             'Services\Controller\CallsForDriver' => 'Services\Controller\CallsForDriverController',
             'Services\Controller\PriceRegion' => 'Services\Controller\PriceRegionController',
             'Services\Controller\AppClosed' => 'Services\Controller\AppClosedController',
             'Services\Controller\Tokens' => 'Services\Controller\TokensController',
             'Services\Controller\ClientCoOrdinate' => 'Services\Controller\ClientCoOrdinateController',
             'Services\Controller\DriverCoOrdinate' => 'Services\Controller\DriverCoOrdinateController',
             'Services\Controller\AppClosed' => 'Services\Controller\AppClosedController',
             'Services\Controller\Towns' => 'Services\Controller\TownsController',
        ),
    ),
    
    'router' => array(
        'routes' => array(
/*
            'requests' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/requests[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Requests\Controller\Requests',
                        'action'     => 'index',
                    ),
                ),
            ),
*/
            'restful-calls' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/calls',
            		'defaults' => array(
            			'controller' => 'Services\Controller\Calls',	
            		),
            	),
            ),
            'restful-sendcall' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/sendcall',
            		'defaults' => array(
            			'controller' => 'Services\Controller\SendCall',	
            		),
            	),
            ),
            'restful-ratings' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/ratings',
            		'defaults' => array(
            			'controller' => 'Services\Controller\Ratings',	
            		),
            	),
            ),
            
            'restful-roadClients' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/roadclients',
            		'defaults' => array(
            			'controller' => 'Services\Controller\RoadClients',	
            		),
            	),
            ),
            
            'restful-drivers' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/drivers',
            		'defaults' => array(
            			'controller' => 'Services\Controller\Drivers',	
            		),
            	),
            ),
            
            'restful-driversSC' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/driverssc',
            		'defaults' => array(
            			'controller' => 'Services\Controller\DriversSC',	
            		),
            	),
            ),
            'restful-driversSCExists' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/driversscexists',
            		'defaults' => array(
            			'controller' => 'Services\Controller\DriversSCExists',	
            		),
            	),
            ),
            'restful-driversClosest' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/driversclosest',
            		'defaults' => array(
            			'controller' => 'Services\Controller\DriversClosest',	
            		),
            	),
            ),
            'restful-callsDrivers' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/callsDrivers',
            		'defaults' => array(
            			'controller' => 'Services\Controller\CallsDrivers',	
            		),
            	),
            ),
            'restful-callsForDriver' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/callsForDriver',
            		'defaults' => array(
            			'controller' => 'Services\Controller\CallsForDriver',	
            		),
            	),
            ),
            'restful-priceregion' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/priceregion',
            		'defaults' => array(
            			'controller' => 'Services\Controller\PriceRegion',	
            		),
            	),
            ),
            'restful-tokens' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/tokens',
            		'defaults' => array(
            			'controller' => 'Services\Controller\Tokens',	
            		),
            	),
            ),
            'restful-clientcoo' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/clientcoo',
            		'defaults' => array(
            			'controller' => 'Services\Controller\ClientCoOrdinate',	
            		),
            	),
            ),
            'restful-drivercoo' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/drivercoo',
            		'defaults' => array(
            			'controller' => 'Services\Controller\DriverCoOrdinate',	
            		),
            	),
            ),
            'restful-appclosed' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/appclosed',
            		'defaults' => array(
            			'controller' => 'Services\Controller\AppClosed',	
            		),
            	),
            ),
            'restful-towns' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/towns',
            		'defaults' => array(
            			'controller' => 'Services\Controller\Towns',	
            		),
            	),
            ),
        ),
    ),
    
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);