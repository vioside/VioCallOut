<?php

namespace CallOut\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use CallOut\Model\Token;
use ZendService\Apple\Apns\Client\Message as Client;
use ZendService\Apple\Apns\Message;
use ZendService\Apple\Apns\Message\Alert;
use ZendService\Apple\Apns\Response\Message as Response;
use ZendService\Apple\Apns\Exception\RuntimeException;

class StatisticsController extends AbstractActionController
{
	
	protected $reportTable;
	public function getReportTable()
    {
        if (!$this->reportTable) {
        	$sm = $this->getServiceLocator();
            $this->reportTable = $sm->get('CallOut\Model\ReportTable');
        }
        return $this->reportTable;
    }

    public function indexAction()
    {
    	if (!$this->zfcUserAuthentication()->hasIdentity()) {
		    return $this->redirect()->toRoute('zfcuser/login');
		}
		$this->layout()->user = $this->zfcUserAuthentication()->getIdentity();
		$this->layout('layout/layout-callout');
		$tokens = $this->getReportTable()->fetchAll();
    	    	
        return new ViewModel(array('reports'=>$tokens));
    }
	
}

