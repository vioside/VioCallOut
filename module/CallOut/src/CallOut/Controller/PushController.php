<?php

namespace CallOut\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use CallOut\Model\Token;
use ZendService\Apple\Apns\Client\Message as Client;
use ZendService\Apple\Apns\Message;
use ZendService\Apple\Apns\Message\Alert;
use ZendService\Apple\Apns\Response\Message as Response;
use ZendService\Apple\Apns\Exception\RuntimeException;

class PushController extends AbstractActionController
{
	protected $appleTokenTable;
	
	public function getAppleTokenTable()
    {
        if (!$this->appleTokenTable) {
            $sm = $this->getServiceLocator();
            $this->appleTokenTable = $sm->get('CallOut\Model\AppleTokenTable');
        }
        return $this->appleTokenTable;
    }
    
    protected $googleTokenTable;
	
	public function getGoogleTokenTable()
    {
        if (!$this->googleTokenTable) {
            $sm = $this->getServiceLocator();
            $this->googleTokenTable = $sm->get('CallOut\Model\GoogleTokenTable');
        }
        return $this->googleTokenTable;
    }
    
    public function indexAction()
    {
    	if (!$this->zfcUserAuthentication()->hasIdentity()) {
		    return $this->redirect()->toRoute('zfcuser/login');
		}
		$this->layout()->user = $this->zfcUserAuthentication()->getIdentity();
		$this->layout('layout/layout-callout');
		$tokens = $this->getAppleTokenTable()->fetchActive();
    	    	
        return new ViewModel(array('tokens'=>$tokens));
    }
	
	public function iospushAction()
	{
		if (!$this->zfcUserAuthentication()->hasIdentity()) {
		    return $this->redirect()->toRoute('zfcuser/login');
		}
		$this->layout('layout/layout-callout');
		
		if($_POST) {
			$alert = $_POST["alert"];
			$sound = $_POST["sound"];
			$badge = $_POST["badge"];
			$id = $_POST["idInput"];
			$tag = $_POST["tag"];
			
			$this->getAppleTokenTable()->broadcastApnsNotification($this->getServiceLocator(), $alert, $sound, $badge, $id, $tag);			
		}
        return new ViewModel(array());
	}

	public function androidpushAction()
	{
		if (!$this->zfcUserAuthentication()->hasIdentity()) {
		    return $this->redirect()->toRoute('zfcuser/login');
		}
		
		$this->layout('layout/layout-callout');
		if($_POST) {
			$alert = $_POST["alert"];
			$id = $_POST["id"];
			$tag = $_POST["tag"];
			
			$this->getGoogleTokenTable()->broadcastGcmNotification($this->getServiceLocator(), $alert, $id, $tag);
			
		}
        return new ViewModel(array());
	}
}

