<?php

namespace CallOut\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use CallOut\Model\Token;

class TokensServiceController extends AbstractRestfulController
{
	protected $appleTokenTable;
	
	public function getAppleTokenTable()
    {
        if (!$this->appleTokenTable) {
            $sm = $this->getServiceLocator();
            $this->appleTokenTable = $sm->get('CallOut\Model\AppleTokenTable');
        }
        return $this->appleTokenTable;
    }
    
    protected $googleTokenTable;
	
	public function getGoogleTokenTable()
    {
        if (!$this->googleTokenTable) {
            $sm = $this->getServiceLocator();
            $this->googleTokenTable = $sm->get('CallOut\Model\GoogleTokenTable');
        }
        return $this->googleTokenTable;
    }
    
    public function getList(){
		$data = array();
/*
		$menuItems = $this->getMenuItemTable()->fetchAll();
		$i = 0;
		foreach ($menuItems as $menuItem) {
			$data[$i] = $menuItem;
			$i = $i + 1;
		}
*/

		return new JsonModel( $data );
	}
	
	public function get($id) {
/*
		$menuItems = $this->getMenuItemTable()->getMenuItem($id);
		$data = array('menu'=>$menuItems);
*/
		$data = null;
		return new JsonModel($data);
	}
	
	public function create($data) { 
		$token = new Token();
		if(array_key_exists("username", $data)) $token->username = $data["username"];
		if(array_key_exists("token", $data)) $token->token = $data["token"];
		if(array_key_exists("ios", $data)) $token->ios = $data["ios"];
		if(array_key_exists("tags", $data)) $token->tags = $data["tags"];
		if(array_key_exists("active", $data)) $token->active = $data["active"];
		
		if($token->ios==1 || $token->ios=="1") {
			$t = $this->getAppleTokenTable()->getTokenByToken($token->token);
			if($t != null) {
				$token->id = $t->id;	
			}
			$token->id = $this->getAppleTokenTable()->saveToken($token);
		} else {
			$t = $this->getGoogleTokenTable()->getTokenByToken($token->token);
			if($t != null) {
				$token->id = $t->id;	
			}
			$token->id = $this->getGoogleTokenTable()->saveToken($token);		
		}
		return new JsonModel(array('token'=>$token));
	}
	
	public function update($id, $data) {
		
/*
		$menuItem = $this->getMenuItemTable()->getMenuItem($id);
		if(array_key_exists("title", $data)) $menuItems->title = $data["title"];
		if(array_key_exists("desc", $data)) $menuItems->desc = $data["desc"];
		if(array_key_exists("price", $data)) $menuItems->price = $data["price"];
		if(array_key_exists("restaurantId", $data)) $menuItems->restaurantId = $data["restaurantId"];
		if(array_key_exists("foodCategoryId", $data)) $menuItems->foodCategoryId = $data["foodCategoryId"];
		if(array_key_exists("lastChangedDate", $data)) $menuItems->lastChangedDate = $data["lastChangedDate"];
		if(array_key_exists("lastChangedName", $data)) $menuItems->lastChangedName = $data["lastChangedName"];

		$this->getMenuItemTable()->saveMenuItem($menuItem);
		
		$d = array('menu' => $menuItem);
		return new JsonModel($d);
*/

	}
	
	public function delete($id) { }


}

