<?php
namespace CallOut\Model;

use Zend\InputFilter\Factory as InputFactory;    
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;   
use Zend\InputFilter\InputFilterInterface;  

class Token implements InputFilterAwareInterface
{
    public $id;
    public $username;
    public $token;
    public $ios;
    public $tags;
    public $active;

	protected $inputFilter;
	
    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->username = (isset($data['username'])) ? $data['username'] : null;
        $this->token = (isset($data['token'])) ? $data['token'] : null;
        $this->ios = (isset($data['ios'])) ? $data['ios'] : null;
        $this->active = (isset($data['active'])) ? $data['active'] : null;
        $this->tags = (isset($data['tags'])) ? $data['tags'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}