<?php
namespace CallOut\Model;

use Zend\Db\TableGateway\TableGateway;

use ZendService\Google\Gcm\Client;
use ZendService\Google\Gcm\Message;
use ZendService\Google\Exception\RuntimeException;
use \DateTime;

class GoogleTokenTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    protected $reportTable;
	public function getReportTable($sm)
    {
        if (!$this->reportTable) {
            $this->reportTable = $sm->get('CallOut\Model\ReportTable');
        }
        return $this->reportTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function fetchActive()
    {
        $resultSet = $this->tableGateway->select(array('active'=>1));
        return $resultSet;
    }
    
    public function fetchGcmActive()
    {
        $resultSet = $this->tableGateway->select(array('active'=>1,'ios'=>0));
        return $resultSet;
    }
    
    public function fetchGcmActiveWithTag($tag)
    {
        $resultSet = $this->tableGateway->select(array('active'=>1, 'tags'=>$tag, 'ios'=>0));
        return $resultSet;
    }
    
    public function getToken($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    } 
    
    public function getTokenByUsername($username)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('username' => $id));
        $row = $rowset->current();
        if (!$row) {
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    } 
    
    public function getTokenByToken($token)
    {
        $rowset = $this->tableGateway->select(array('token' => $token));
        $row = $rowset->current();
        if (!$row) {
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    } 
    
    public function saveToken(Token $token)
    {
        $data = array(
        	'id' => $token->id,
            'username' => $token->username,
            'token'  => $token->token,
            'ios'  => $token->ios,
            'active'  => $token->active,
            'lastchanged'  => time(),
            'tags'  => $token->tags,
        );

        $id = (int)$token->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getToken($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
    public function broadcastGcmNotification($sm, $alert, $id, $tag)
    {
		$tokens = null;
		if($tag != null) $tokens = $this->fetchGmcActiveWithTag($tag);
		else $tokens = $this->fetchGcmActive();

		
		$gcm_text = $alert;
		$gcm_id = $id;
		
		$noSent = 0;
		$sql = "select gcm_response_token as regId from GCM";
		
		foreach($tokens as $token) {
			$gcm_array[]=$token->token;
			$noSent++;
		}
		
		$chunk_result = array_chunk($gcm_array, 100, true); 
		foreach($chunk_result as $chunk) { 
		    $body = array(
		      'gcmText' => $gcm_text
		    );
		    if($gcm_id != null) {
			    $body = array(
			      'gcmText' => $gcm_text,
			      'gcmId' => $gcm_id,
			    );
		    }
			
			    // Set POST variables
			  $url = 'https://android.googleapis.com/gcm/send';
			
			  $fields = array(
			 'registration_ids' => $chunk,
			 'data' => $body,
			 );
			
			//echo GOOGLE_API_KEY;
			$apiKey='AIzaSyCzNYc3hlLpNpOU-IqO94oEWLcXqJNKMnA';
			 $headers = array(
			 "Authorization: key=$apiKey",
			  'Content-Type: application/json'
			 );
			
			
			print_r($headers);
			// Open connection
			$ch = curl_init();
			
			// Set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
			
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			// Execute post
		
			$result = curl_exec($ch);
			if ($result === FALSE) {
			    die('Curl failed: ' . curl_error($ch));
			}
		
			
			// Close connection
			curl_close($ch);
			echo $result;
		}
		
		$report = new Report();
		$report->date = date("Y-m-d H:i:s");
		$report->noSent = $noSent;
		$report->ios = 0;
		$report->token = $alert;
		$report->invalid = 0;
		$this->getReportTable($sm)->saveReport($report);

	}



}