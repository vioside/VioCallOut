<?php
namespace CallOut\Model;

use Zend\Db\TableGateway\TableGateway;

class ReportTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getReport($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    } 
    
    
    public function saveReport(Report $token)
    {
        $data = array(
        	'id' => $token->id,
            'date' => $token->date,
            'noSent'  => $token->noSent,
            'ios'  => $token->ios,
            'token'  => $token->token,
            'invalid'  => $token->invalid,
        );

        $id = (int)$token->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getToken($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
}