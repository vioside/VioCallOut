<?php
namespace CallOut\Model;

use Zend\Db\TableGateway\TableGateway;
use ZendService\Apple\Apns\Client\Message as Client;
use ZendService\Apple\Apns\Message;
use ZendService\Apple\Apns\Message\Alert;
use ZendService\Apple\Apns\Response\Message as Response;
use ZendService\Apple\Apns\Exception\RuntimeException;
use \DateTime;

class AppleTokenTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function fetchActive()
    {
        $resultSet = $this->tableGateway->select(array('active'=>1));
        return $resultSet;
    }
    
    public function fetchApnsActive()
    {
        $resultSet = $this->tableGateway->select(array('active'=>1,'ios'=>1));
        return $resultSet;
    }
    
    public function fetchApnsActiveWithTag($tag)
    {
        $resultSet = $this->tableGateway->select(array('active'=>1, 'tags'=>$tag, 'ios'=>1));
        return $resultSet;
    }
    
    public function getToken($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    } 
    
    public function getTokenByUsername($username)
    {
        $rowset = $this->tableGateway->select(array('username' => $username));
        $row = $rowset->current();
        if (!$row) {
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    } 
    
    public function getTokenByToken($token)
    {
        $rowset = $this->tableGateway->select(array('token' => $token));
        $row = $rowset->current();
        if (!$row) {
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    } 	
    
    protected $reportTable;
	public function getReportTable($sm)
    {
        if (!$this->reportTable) {
            $this->reportTable = $sm->get('CallOut\Model\ReportTable');
        }
        return $this->reportTable;
    }
    
    public function saveToken(Token $token)
    {
        $data = array(
        	'id' => $token->id,
            'username' => $token->username,
            'token'  => $token->token,
            'ios'  => $token->ios,
            'active'  => $token->active,
            'lastchanged'  => time(),
            'tags'  => $token->tags,
        );

        $id = (int)$token->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getToken($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
    public function deleteToken($id) {
        $this->tableGateway->delete(array('id' => $id));
    }
    
    public function broadcastApnsNotification($sm, $alert, $sound, $badge, $id, $tag)
    {
		$tokens = null;
		if($tag != null) $tokens = $this->fetchApnsActiveWithTag($tag);
		else $tokens = $this->fetchApnsActive();
		
		$client = new Client();
		$client->open(Client::SANDBOX_URI, './module/CallOut/src/CallOut/Certificates/DevCertificate.pem', '');
		//$client->open(Client::PRODUCTION_URI, './module/CallOut/src/CallOut/Certificates/ProCertificate.pem', '');
		$noSent = 0;
		$invalid = 0;
		foreach($tokens as $token) {
			
			$message = new Message();
			$message->setId($id);
			$message->setToken(str_replace("-","",$token->token));
			$message->setBadge(intval($badge));
			$message->setSound($sound);
			
			if((isset($id) && $id!="") || isset($tag)) {
				//complex alert
				$al = new Alert();
				$al->setBody($alert . '');
				$al->setActionLocKey('PLAY');
				$al->setLocKey($alert . '');
				$al->setLocArgs(array($id));
				$al->setLaunchImage('Play.png');
				$message->setAlert($al);
			} else {
				//simple alert
				$message->setAlert($alert);
			}
	
	
			try {
			    $response = $client->send($message);
			} catch (RuntimeException $e) {
			    echo $e->getMessage() . PHP_EOL;
			    exit(1);
			}
			
			$responsecode = $response->getCode();
			if ($responsecode != Response::RESULT_OK) {
			     switch ($responsecode) {
			         case Response::RESULT_PROCESSING_ERROR:
			             // you may want to retry
			             break;
			         case Response::RESULT_MISSING_TOKEN:
			             // you were missing a token
			             break;
			         case Response::RESULT_MISSING_TOPIC:
			             // you are missing a message id
			             break;
			         case Response::RESULT_MISSING_PAYLOAD:
			             // you need to send a payload
			             break;
			         case Response::RESULT_INVALID_TOKEN_SIZE:
			             // the token provided was not of the proper size
			             break;
			         case Response::RESULT_INVALID_TOPIC_SIZE:
			             // the topic was too long
			             break;
			         case Response::RESULT_INVALID_PAYLOAD_SIZE:
			             // the payload was too large
			             break;
			         case Response::RESULT_INVALID_TOKEN:
			             // the token was invalid; remove it from your system
			             $this->deleteToken($token->id);
			             $invalid++;
			             break;
			         case Response::RESULT_UNKNOWN_ERROR:
			             // apple didn't tell us what happened
			             break;
			     }
		     } else {
		     	$noSent++;
		     }
		
			
		}
		$client->close();
			
		$report = new Report();
		$report->date = date("Y-m-d H:i:s");
		$report->noSent = $noSent;
		$report->ios = 1;
		$report->token = $alert;
		$report->invalid = $invalid;
		$this->getReportTable($sm)->saveReport($report);

    }
        
    public function sendApnsNotification($token, $alert, $sound, $badge, $id, $tag)
    {
		$tokens = null;
		if($tag != null) $tokens = $this->fetchApnsActiveWithTag($tag);
		else $tokens = $this->fetchApnsActive();
		
		$client = new Client();
		$client->open(Client::SANDBOX_URI, './module/CallOut/src/CallOut/Certificates/DevCertificate.pem', '');
		//$client->open(Client::PRODUCTION_URI, './module/CallOut/src/CallOut/Certificates/ProCertificate.pem', '');
			
		$message = new Message();
		$message->setId($id);
		$message->setToken(str_replace("-","",$token->token));
		$message->setBadge(intval($badge));
		$message->setSound($sound);
		
			if((isset($id) && $id!="") || isset($tag)) {
				//complex alert
				$al = new Alert();
				$al->setBody($alert . '');
				$al->setActionLocKey('PLAY');
				$al->setLocKey($alert . '');
				$al->setLocArgs(array($id));
				$al->setLaunchImage('Play.png');
				$message->setAlert($al);
			} else {
				//simple alert
				$message->setAlert($alert);
			}


		try {
		    $response = $client->send($message);
		} catch (RuntimeException $e) {
		    echo $e->getMessage() . PHP_EOL;
		    exit(1);
		}
			
		$client->close();
			
		if ($response->getCode() != Response::RESULT_OK) {
		     switch ($response->getCode()) {
		         case Response::RESULT_PROCESSING_ERROR:
		             // you may want to retry
		             break;
		         case Response::RESULT_MISSING_TOKEN:
		             // you were missing a token
		             break;
		         case Response::RESULT_MISSING_TOPIC:
		             // you are missing a message id
		             break;
		         case Response::RESULT_MISSING_PAYLOAD:
		             // you need to send a payload
		             break;
		         case Response::RESULT_INVALID_TOKEN_SIZE:
		             // the token provided was not of the proper size
		             break;
		         case Response::RESULT_INVALID_TOPIC_SIZE:
		             // the topic was too long
		             break;
		         case Response::RESULT_INVALID_PAYLOAD_SIZE:
		             // the payload was too large
		             break;
		         case Response::RESULT_INVALID_TOKEN:
		             // the token was invalid; remove it from your system
		             break;
		         case Response::RESULT_UNKNOWN_ERROR:
		             // apple didn't tell us what happened
		             break;
		     }
		}

    }


}