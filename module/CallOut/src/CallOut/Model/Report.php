<?php
namespace CallOut\Model;

use Zend\InputFilter\Factory as InputFactory;    
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;   
use Zend\InputFilter\InputFilterInterface;  

class Report implements InputFilterAwareInterface
{
    public $id;
    public $date;
    public $noSent;
    public $ios;
    public $token;
    public $invalid;

	protected $inputFilter;
	
    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->date = (isset($data['date'])) ? $data['date'] : null;
        $this->noSent = (isset($data['noSent'])) ? $data['noSent'] : null;
        $this->ios = (isset($data['ios'])) ? $data['ios'] : null;
        $this->token = (isset($data['token'])) ? $data['token'] : null;
        $this->invalid = (isset($data['invalid'])) ? $data['invalid'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}