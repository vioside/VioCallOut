<?php
namespace CallOut;

use CallOut\Model\Token;
use CallOut\Model\AppleTokenTable;
use CallOut\Model\GoogleTokenTable;
use CallOut\Model\ReportTable;
use CallOut\Model\Report;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'CallOut\Model\AppleTokenTable' =>  function($sm) {
                    $tableGateway = $sm->get('AppleTokenTableGateway');
                    $table = new AppleTokenTable($tableGateway);
                    return $table;
                },
                'AppleTokenTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Token());
                    return new TableGateway('token', $dbAdapter, null, $resultSetPrototype);
                },
                'CallOut\Model\GoogleTokenTable' =>  function($sm) {
                    $tableGateway = $sm->get('GoogleTokenTableGateway');
                    $table = new GoogleTokenTable($tableGateway);
                    return $table;
                },
                'GoogleTokenTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Token());
                    return new TableGateway('token', $dbAdapter, null, $resultSetPrototype);
                },
                'CallOut\Model\ReportTable' =>  function($sm) {
                    $tableGateway = $sm->get('ReportTableGateway');
                    $table = new ReportTable($tableGateway);
                    return $table;
                },
                'ReportTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Report());
                    return new TableGateway('calloutreport', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}
