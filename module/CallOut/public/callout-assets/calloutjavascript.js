$(document).ready(function(){
	$(".errors").hide();
	
	$(".iOSTab").click(function(){
     	$(".iOSTab div").addClass("selected");
 		$(".androidTab div").removeClass("selected");
 		$(".iosExtraButtons").show(200);
    });
     
    $(".androidTab").click(function(){
 		$(".androidTab div").addClass("selected");
 		$(".iOSTab div").removeClass("selected");
		$(".iosExtraButtons").hide(200);
    });
});

function validateForm() {
	var success = 1;
	var iOSPush = 1;
	var message = "! - ";
	var i = 0;
	
	if ( $( ".androidTab div" ).hasClass( "selected" ) ) {
		iOSPush = 0;
	}
    
    var alertMessage = document.forms["pushForm"]["alert"].value;
    var sound = document.forms["pushForm"]["sound"].value;
    var badge = document.forms["pushForm"]["badge"].value;
    var idText = document.forms["pushForm"]["id"].value;
    var tagText = document.forms["pushForm"]["tag"].value;
    
    if (alertMessage == null || alertMessage == "") {
        
		if(i > 0){ message += "</br>! - "; }
		i++;

        message += "Alert must be filled out";
        
        $(".alertInput").css({
           "background-color": "#f7eeeb"
     	});
        success = 0;
    }
    else
    {
        $(".alertInput").css({
           "background-color": "#fff"
     	});
    	
    }
    
    if(iOSPush == 1){
	    if (sound == null || sound == "") {
			if(i > 0){ message += "</br>! - "; }
			i++;
	        message += "Sound must be filled out";
	        $(".soundInput").css({
               "background-color": "#f7eeeb"
         	});
        	success = 0;
	    }
	    else
	    {
	        $(".soundInput").css({
               "background-color": "#fff"
         	});
	    }
	    
	    if (badge == null || badge == "") {
	    
			if(i > 0){ message += "</br>! - "; }
			i++;
	        message += "Badge must be filled out";
	        $(".badgeInput").css({
               "background-color": "#f7eeeb"
         	});
        	success = 0;
	    }
	    else if(isNaN(parseInt(badge)))
	    {
			if(i > 0){ message += "</br>"; }
			i++;
	        message += "Badge must be a number";
	        $(".badgeInput").css({
               "background-color": "#f7eeeb"
         	});
        	success = 0;
	    }
	    else{
	        $(".badgeInput").css({
               "background-color": "#fff"
         	});
     	}
    }
    	    
    if (idText == null || idText == "") {
		if(i > 0){ message += "</br>! - "; }
		i++;
        message += "ID must be filled out";
        $(".idInput").css({
           "background-color": "#f7eeeb"
     	});
        success = 0;
    }
    else{
        $(".idInput").css({
           "background-color": "#fff"
     	});
    }
    
    if (tagText == null || tagText == "") {
		if(i > 0){ message += "</br>! - "; }
		i++;
        message += "Tag must be filled out";
        $(".tagInput").css({
           "background-color": "#f7eeeb"
     	});
        success = 0;
    }
    else{
        $(".tagInput").css({
           "background-color": "#fff"
     	});
 	}
    
    if(success == 0)
    {
		$(".errors").fadeIn(500);
		
		$(".errors p").html(message);
		
	    return false;
    }
}