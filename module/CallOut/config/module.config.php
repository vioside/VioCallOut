<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'CallOut\Controller\Push' => 'CallOut\Controller\PushController',
            'CallOut\Controller\TokensService' => 'CallOut\Controller\TokensServiceController',
            'CallOut\Controller\Statistics' => 'CallOut\Controller\StatisticsController',
        ),
    ),
    
    'router' => array(
        'routes' => array(
            'push' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/callout[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'CallOut\Controller\Push',
                        'action'     => 'index',
                    ),
                ),
            ),
            'statistics' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/statistics[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'CallOut\Controller\Statistics',
                        'action'     => 'index',
                    ),
                ),
            ),
            'restful-tokens' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/tokens',
            		'defaults' => array(
            			'controller' => 'CallOut\Controller\TokensService',	
            		),
            	),
            ),  
            'restful-push' => array(
            	'type' => 'Literal',
            	'options' => array(
            		'route' => '/rest/push',
            		'defaults' => array(
            			'controller' => 'CallOut\Controller\PushService',	
            		),
            	),
            ),       
		),
    ),
    
    'view_manager' => array(
        'template_path_stack' => array(
            'push' => __DIR__ . '/../view',
            'zfcuser' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);